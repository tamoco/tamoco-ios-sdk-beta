//
//  Tamoco.h
//  Tamoco
//
//  Created by Sašo Sečnjak on 19/04/16.
//  Copyright © 2016 Tamoco. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Tamoco.
FOUNDATION_EXPORT double TamocoVersionNumber;

//! Project version string for Tamoco.
FOUNDATION_EXPORT const unsigned char TamocoVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Tamoco/PublicHeader.h>


#import "NetworkTools.h"
#import "Crypto.h"
#include <ifaddrs.h>

