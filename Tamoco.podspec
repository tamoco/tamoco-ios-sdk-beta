Pod::Spec.new do |s|
  s.name         = "Tamoco"
  s.version      = "1.6.2GM6"
  s.summary      = "Tamoco iOS SDK"
  s.description  = "Tamoco iOS SDK Binary SDK for integrating proximity awareness"
  s.homepage     = "https://www.tamoco.com"
  s.license      = "Apache License, Version 2.0"
  s.author       = { "Daniel Angel" => "daniel.angel@tamo.co" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => 'https://bitbucket.org/tamoco/tamoco-ios-privacysdk.git', :tag => "#{s.version}" }
  s.ios.vendored_frameworks = 'Tamoco.framework'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.0' }
  s.compiler_flags = '-swift-version 5.0'
  s.static_framework = true
  s.dependency 'Sentry', '~> 4.3.4'
end
